# SamKnows Backend Test

## Installation

To get started clone the repository and install the application dependencies.

```
git clone https://bitbucket.org/davidkane/samknows-test.git
cd samknows-test
composer install
```

Next you need to copy the file `config/config.sample.php` and rename it to `config.php` in the same location, you can do this by running the following command:

```
cp config/config.sample.php config/config.php 
```

Inside this file you will need to set your database configuration including your database name. Once this is done the final step is to create the table structures within the database, which can be done automatically but running the setup command of the application:

```
php app.php setup 
```

If all was successful you should be good to go.

## Usage

### Import data

```
php app.php 
```

### Produce data report

```
php app.php report [unit_id] [metric] [hour_of_day]
```

The above command required 3 parameters, the unit you wish to report on, the metric you wish to report on and the hour of the day you wish to report on. When you replace these with the values you want you should get a command like the following example to run:

```
php app.php report 1 upload 19
```

This will produce the follow report:

```
Generating Report for Unit 1 on metric upload at 19:
MEAN: 973,326.13
MIN: 110,192
MAX: 2,323,370
MEDIAN: 1,204,760.00
SAMPLE SIZE: 97
```

## Future Improvements give more time

* Add unit tests to the application
* Add more error catches / validation i.e. when config not set / mysql errors / poorly formatted parameters on report command etc.
* Store report values into database for each hour / metric
