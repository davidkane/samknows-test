<?php

return array(
    'db' => array(
        'host'     => 'xxxxx',
        'name'     => 'xxxxx',
        'username' => 'xxxxx',
        'password' => 'xxxxx',
        'options'  => array( PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'" ),
        'timezone' => 'UTC'
    )
);