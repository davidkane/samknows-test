<?php

namespace SamKnows\Commands;

/**
 * DataImporter processes data from JSON into aggregated tables
 *
 * @package  SamKnows\Commands
 * @author   David Kane <dk8209@gmail.com>
 * @version  1.0.0
 */
class DataImporter extends BaseCommand implements CommandInterface {
    // Location of data points
    private $dataUrl = 'http://tech-test.sandbox.samknows.com/php-2.0/testdata.json';

    // Stored data fetch from endpoint
    private $data;

    /**
     * DataImporter constructor
     *
     * @param array $config
     */
    public function __construct($config) {
        parent::__construct( $config );
    }

    /**
     * Method which processes the data metrics
     *
     * @return void
     */
    public function run() {
        // Fetch data
        $this->data = $this->getData();

        // Process fetched data
        $this->processData();
    }

    /**
     * Method which gets data from endpoint
     *
     * @return array
     */
    private function getData() {
        // Use cURL to fetch the data from the endpoint
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_URL, $this->dataUrl );
        $data = curl_exec( $ch );
        curl_close( $ch );

        // Decode data
        $data = json_decode( $data );

        // Check data is valid JSON
        if( $data === null ) {
            die( "Data invalid, Import aborted" );
        }

        // Return decoded data
        return $data;
    }

    /**
     * Method which processes the data into aggregated tables
     *
     * @return array
     */
    private function processData() {
        // Connect to database
        $this->db = $this->connectToDb();

        // Check for successful connection
        if( !$this->db ) {
            die( "Connection to database failed, aborting" );
        }

        // Loop and process units from data
        foreach( $this->data as $data ) {
            // Get Unit ID
            $unitId = isset( $data->unit_id ) ? (int) $data->unit_id : null;

            // Validate Unit ID
            if( $unitId === null || $unitId === 0 ) {
                echo "No unit ID found.\n";
                continue;
            }

            // Process unit
            $this->processUnit( $unitId );

            // Loop and process metrics
            echo "Processing metrics for Unit {$unitId}...\n";
            foreach( $data->metrics as $key => $value ) {
                $this->processMetric( $key, $value, $unitId );
            }
        }
    }

    /**
     * Method insert / update units
     *
     * @param int $unitId
     *
     * @return void
     */
    private function processUnit($unitId) {
        // Prepare Query
        $query = $this->db->prepare( "INSERT INTO units (id)  VALUES (:id) ON DUPLICATE KEY UPDATE id=:id" );

        // Execute query
        try {
            $query->execute( array( ':id' => $unitId ) );
        } catch( \PDOException $e ) {
            echo "Caught exception: {$e->getMessage()}\n";
        }

    }

    /**
     * Method insert / update metrics
     *
     * @param string $metric
     * @param array  $values
     * @param int    $unitId
     *
     * @return void
     */
    private function processMetric($metric, $values, $unitId) {
        // Prepare Query
        $query = $this->db->prepare( "INSERT INTO {$metric} (unit_id, timestamp, value)  VALUES (:unit_id, :timestamp, :value) ON DUPLICATE KEY UPDATE value=:value" );

        // Loop and execute values
        try {
            foreach( $values as $value ) {
                $query->execute( array( ':unit_id' => $unitId, ':timestamp' => $value->timestamp, ':value' => $value->value ) );
            }
        } catch( \PDOException $e ) {
            echo "Caught exception: {$e->getMessage()}\n";
        }

    }
}