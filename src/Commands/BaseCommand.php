<?php

namespace SamKnows\Commands;

use Supadu\PDOManager\PDOManager;

/**
 * BaseCommand contains properties and methods to be inherited by each Command
 *
 * @package  SamKnows\Commands
 * @author   David Kane <dk8209@gmail.com>
 * @version  1.0.0
 */
class BaseCommand {
    // Database connection details
    private $dbConfig;

    // Database connection
    protected $db;

    /**
     * BaseCommand constructor
     *
     * @param array $config
     */
    public function __construct($config) {
        $this->dbConfig = $config;
    }

    /**
     * Method connects to the database and returns the PDOManager
     *
     * @return PDOManager
     */
    protected function connectToDb() {
        return new PDOManager( 'mysql:host=' . $this->dbConfig['host'] . ';dbname=' . $this->dbConfig['name'], $this->dbConfig['username'], $this->dbConfig['password'], $this->dbConfig['options'], $this->dbConfig['timezone'] );
    }
}