<?php

namespace SamKnows\Commands;

/**
 * DatabaseCreater setups up the aggregated tables
 *
 * @package  SamKnows\Commands
 * @author   David Kane <dk8209@gmail.com>
 * @version  1.0.0
 */
class DatabaseCreator extends BaseCommand implements CommandInterface {
    private $unitsTable = 'CREATE TABLE `units` (
          `id` int(11) unsigned NOT NULL,
          PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;';

    private $downloadTable = 'CREATE TABLE `download` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `unit_id` int(11) unsigned DEFAULT NULL,
          `timestamp` timestamp NULL DEFAULT NULL,
          `value` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `UNIQUE` (`unit_id`,`timestamp`),
          CONSTRAINT `download_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=2337 DEFAULT CHARSET=utf8;';

    private $uploadTable = 'CREATE TABLE `upload` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `unit_id` int(11) unsigned DEFAULT NULL,
          `timestamp` timestamp NULL DEFAULT NULL,
          `value` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `UNIQUE` (`unit_id`,`timestamp`),
          CONSTRAINT `upload_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=2209 DEFAULT CHARSET=utf8;';

    private $latencyTable = 'CREATE TABLE `latency` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `unit_id` int(11) unsigned DEFAULT NULL,
          `timestamp` timestamp NULL DEFAULT NULL,
          `value` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `UNIQUE` (`unit_id`,`timestamp`),
          CONSTRAINT `latency_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=10015 DEFAULT CHARSET=utf8;';

    private $packetLossTable = 'CREATE TABLE `packet_loss` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `unit_id` int(11) unsigned DEFAULT NULL,
          `timestamp` timestamp NULL DEFAULT NULL,
          `value` float DEFAULT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `UNIQUE` (`unit_id`,`timestamp`),
          CONSTRAINT `packet_loss_ibfk_1` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB AUTO_INCREMENT=10015 DEFAULT CHARSET=utf8;';

    /**
     * DataReporter constructor
     *
     * @param array  $config
     */
    public function __construct($config) {
        parent::__construct( $config );
    }

    /**
     * Method which processes the data metrics
     *
     * @return void
     */
    public function run() {
        // Connect to database
        $this->db = $this->connectToDb();

        // Create Units table
        $this->createTable( 'unitsTable');

        // Create Download table
        $this->createTable('downloadTable');

        // Create Upload table
        $this->createTable('uploadTable');

        // Create Latency table
        $this->createTable('latencyTable');

        // Create PacketLoss table
        $this->createTable('packetLossTable');
    }


    /**
     * Method to create table
     *
     * @param string $table
     *
     * @return void
     */
    private function createTable($table) {
        // Prepare Query
        $query = $this->db->prepare( $this->{$table} );

        // Execute query
        try {
            $query->execute();
        } catch( \PDOException $e ) {
            echo "Caught exception: {$e->getMessage()}\n";
        }

    }
}