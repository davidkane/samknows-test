<?php

namespace SamKnows\Commands;

/**
 * Interface CommandInterface specifics methods all commands much implement
 *
 * @package SamKnows\Commands
 */
interface CommandInterface {
    public function run();
}