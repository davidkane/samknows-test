<?php

namespace SamKnows\Commands;

/**
 * DataReporter processes data from aggregated tables to produce reports
 *
 * @package  SamKnows\Commands
 * @author   David Kane <dk8209@gmail.com>
 * @version  1.0.0
 */
class DataReporter extends BaseCommand implements CommandInterface {
    // Stores data sample mean for report
    private $sampleMean;

    // Stores data sample min for report
    private $sampleMin;

    // Stores data sample max for report
    private $sampleMax;

    // Stores data sample median for report
    private $sampleMedian;

    // Stores data sample size for report
    private $sampleSize;

    // Unit ID to generate report for
    private $unitId;

    // Metric to generate report for
    private $metric;

    // Hour of the day to report on
    private $hour;

    /**
     * DataReporter constructor
     *
     * @param array  $config
     * @param string $unitId
     * @param string $metric
     * @param string $hour
     */
    public function __construct($config, $unitId, $metric, $hour) {
        parent::__construct( $config );

        $this->unitId = (int) $unitId;
        $this->metric = $metric;
        $this->hour = (int) $hour;
    }

    /**
     * Method which processes the data metrics
     *
     * @return void
     */
    public function run() {
        echo "Generating Report for Unit {$this->unitId} on metric {$this->metric} at {$this->hour}:\n\n";

        // Get calculate values
        $this->calculateValues();
        $this->calculateMedian();

        // Print report
        echo "MEAN: {$this->sampleMean}\n";
        echo "MIN: {$this->sampleMin}\n";
        echo "MAX: {$this->sampleMax}\n";
        echo "MEDIAN: {$this->sampleMedian}\n";
        echo "SAMPLE SIZE: {$this->sampleSize}\n";
    }

    /**
     * Method to calculate values for report
     *
     * @return void
     */
    private function calculateValues() {
        // Connect to database
        $this->db = $this->connectToDb();

        // Prepare Query
        $query = $this->db->prepare( "SELECT COUNT(value) as sampleSize, AVG(value) as mean, MIN(value) as min, MAX(value) as max FROM {$this->metric} WHERE HOUR(timestamp) = :hour" );

        // Execute query
        try {
            $query->execute( array( ':hour' => $this->hour ) );
            $row = $query->fetch();
            $this->sampleMean = number_format( $row['mean'], 2 );
            $this->sampleMin = number_format( $row['min'] );
            $this->sampleMax = number_format( $row['max']);
            $this->sampleSize = number_format( $row['sampleSize']);
        } catch( \PDOException $e ) {
            echo "Caught exception: {$e->getMessage()}\n";
        }

    }

    /**
     * Method to calculate median value for report
     *
     * @return void
     */
    private function calculateMedian() {
        // Calculate offset and limit
        if( ( $this->sampleSize % 2 ) == 1 ) {
            $limit = 1;
            $offset = floor( $this->sampleSize / 2 );
        } else {
            $limit = 2;
            $offset = ( $this->sampleSize / 2 ) - 1;
        }


        // Prepare Query
        $query = $this->db->prepare( "SELECT value FROM {$this->metric} WHERE HOUR(timestamp) = :hour ORDER BY value DESC LIMIT {$limit} OFFSET {$offset};" );

        // Execute query
        try {
            $query->execute( array( ':hour' => $this->hour ) );
            $rows = $query->fetchAll();
            $median = 0;

            foreach( $rows as $row ) {
                $median += $row['value'];
            }

            $this->sampleMedian = number_format( $median / count( $rows ), 2 );
        } catch( \PDOException $e ) {
            echo "Caught exception: {$e->getMessage()}\n";
        }

    }
}