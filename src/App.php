<?php

namespace SamKnows;

use SamKnows\Commands\DataImporter;
use SamKnows\Commands\DataReporter;
use SamKnows\Commands\DatabaseCreator;

/**
 * Main application
 *
 * @package  SamKnows
 * @author   David Kane <dk8209@gmail.com>
 * @version  1.0.0
 */
class App {
    // Application config
    private $config;

    /**
     * App constructor
     */
    public function __construct() {
        $configPath = __DIR__ . '/../config/config.php';

        if( !file_exists( $configPath ) ) {
            die( "Config not found, aborting" );
        }

        // Get config
        $this->config = require_once $configPath;
    }

    /**
     * Method which processes the running of the application
     *
     * @param $argv
     *
     * @return void
     */
    public function run($argv) {
        // If not command is provided default to import
        $command = isset( $argv[1] ) ? $argv[1] : 'import';

        // Instantiate command
        if( $command === 'import' ) {
            $command = new DataImporter( $this->config['db'] );
        } elseif( $command === 'report' ) {
            if( !isset( $argv[2] ) ) {
                die( "No Unit ID to report provided.\n" );
            }

            if( !isset( $argv[3] ) ) {
                die( "No metric to report provided.\n" );
            }

            if( !isset( $argv[4] ) ) {
                die( "No hour to report provided.\n" );
            }
            $command = new DataReporter( $this->config['db'], $argv[2], $argv[3], $argv[4] );
        } elseif( $command === 'setup' ) {
            $command = new DatabaseCreator( $this->config['db'] );
        } else {
            die( "Command not recognised.\n" );
        }

        // Run command
        $command->run();
    }
}