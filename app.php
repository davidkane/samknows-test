<?php

/**
 * Application Bootstrapper
 */

// Require composer autoloader
require __DIR__ . '/vendor/autoload.php';

// State used classes
use SamKnows\App as SamKnows;

try {
    // Instantiate SamKnows application and run passing command line arguements through
    $application = new SamKnows();
    $application->run( $argv );
} catch( Exception $e ) {
    echo "Caught exception: {$e->getMessage()}\n";
}